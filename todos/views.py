from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoFormItems
# Create your views here.

def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_details = TodoList.objects.get(id=id)
    context = {
        "todo_details": todo_details,

    }
    return render(request,"todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail",id=todo_list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm(instance=todo_list)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        todo_item = TodoFormItems(request.POST)
        if todo_item.is_valid():
            todo_item = todo_item.save()

            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoFormItems()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)

def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        todo_item = TodoFormItems(request.POST, instance=todo_item)
        if todo_item.is_valid():
            todo_item.save()
            return redirect("todo_list_detail", id=todo_item.id)
    else:
        form = TodoFormItems(instance=todo_item)
    context = {
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
